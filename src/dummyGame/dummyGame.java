
package dummyGame;

import dummyGame.HUD.Hud;
import java.util.List;
import java.util.Map;
import org.joml.Vector2f;
import org.joml.Vector3f;
import static org.lwjgl.glfw.GLFW.*;
import oxygenengine.Renderer;
import oxygenengine.Window;
import oxygenengine.items.GameItem;
import oxygenengine.graph.Camera;
import oxygenengine.graph.Material;
import oxygenengine.graph.Mesh;
import oxygenengine.graph.OBJloader;
import oxygenengine.graph.Texture;
import oxygenengine.graph.lighting.DirectionalLight;
import oxygenengine.MouseInput;
import oxygenengine.Scene;
import oxygenengine.items.SkyBox;
import oxygenengine.graph.lighting.SceneLight;
import oxygenengine.IGameLogic;


/**
 *
 * @author zeroxgen
 */
public class dummyGame implements IGameLogic{
    
    private Scene scene;
    
    private final Renderer renderer;

    private float m_lightAngle;
    
    private final Camera m_camera;
    private final Vector3f cameraDelta; //camera position increment
    
    
    private static final float CAMERA_STEP = 0.05f;
    private static final float MOUSE_SENSITIVITY = 0.2f;
    
    private boolean dialog_update = false;
    private boolean inputFlag = false;
    
    private Hud m_hud;
    
    private boolean textUpdate = false;
        
    public dummyGame(){
        renderer = new Renderer();
        m_camera = new Camera();
        cameraDelta = new Vector3f(0,0,0);
        m_lightAngle = -90;
     }
    
    @Override
    public void init(Window mWindow) throws Exception{
        renderer.init(mWindow);

        scene = new Scene();
        
        float reflectance = 1f;
        
        Mesh mesh = OBJloader.loadMesh("/models/cube.obj");
        Texture texture = new Texture("textures/grassblock.png");
        Material material = new Material(texture, reflectance);
        mesh.setMaterial(material);
        
        float blockScale = 0.5f;
        float skyBoxScale = 10.0f;
        float extension = 2.0f;
        
        float startx = extension * (-skyBoxScale + blockScale);
        float startz = extension * (skyBoxScale - blockScale);
        float starty = -1.0f;
        float inc = blockScale * 2;
        
        float posx = startx;
        float posz = startz;
        float incy = 0.0f;
        int NUM_ROWS = (int)(extension * skyBoxScale * 2 / inc);
        int NUM_COLS = (int)(extension * skyBoxScale * 2/ inc);        

        GameItem[] gameItems  = new GameItem[NUM_ROWS * NUM_COLS];
        for(int i=0; i<NUM_ROWS; i++) {
            for(int j=0; j<NUM_COLS; j++) {
                GameItem gameItem = new GameItem(mesh);
                gameItem.setScale(blockScale);
                incy = Math.random() > 0.9f ? blockScale * 2 : 0f;
                gameItem.setPos(posx, starty + incy, posz);
                gameItems[i*NUM_COLS + j] = gameItem;
                
                posx += inc;
            }
            posx = startx;
            posz -= inc;
        }
        scene.setGameItems(gameItems);

        // Setup  SkyBox
        SkyBox skyBox = new SkyBox("/models/skybox.obj", "textures/skybox.png");
        skyBox.setScale(skyBoxScale);
        scene.setSkyBox(skyBox);
        
        setupLights();
        
        m_hud = new Hud(mWindow ,"DEMO");
        
        m_camera.getPos().x = 0.65f;
        m_camera.getPos().y = 1.15f;
        m_camera.getPos().y = 4.34f;        
    }
    
    @Override
    public void input(Window mWindow, MouseInput mInput){
        
        cameraDelta.set(0,0,0);
        if(mWindow.isKeyPressed(GLFW_KEY_W)){
            cameraDelta.z = -1;
        } else if(mWindow.isKeyPressed(GLFW_KEY_S)){
            cameraDelta.z = 1;
        } else if (mWindow.isKeyPressed(GLFW_KEY_A)){
            cameraDelta.x = -1;
        } else if (mWindow.isKeyPressed(GLFW_KEY_D)){
            cameraDelta.x = 1;
        } else if (mWindow.isKeyPressed(GLFW_KEY_Z)){
            cameraDelta.y = -1;
        } else if (mWindow.isKeyPressed(GLFW_KEY_X)){
            cameraDelta.y = 1;
        }
        else if (mWindow.isKeyPressed(GLFW_KEY_U)) {
            if(dialog_update == false){
                m_hud.setInputFlag(false);
                dialog_update = true;
                try{
                    m_hud.startDialog("test_dialog_0");
                }catch(Exception e){
                    System.out.println("Hello");
                }
            }
        } else if (mWindow.isKeyPressed(GLFW_KEY_I)) {
            if(dialog_update == true){
                inputFlag = true;
            }
        }
        
        
    }

    @Override
    public void update(MouseInput mInput){
        
        //update camera position
        m_camera.movePos(cameraDelta.x * CAMERA_STEP, cameraDelta.y * CAMERA_STEP, cameraDelta.z * CAMERA_STEP);
        
        //update camera rotation
        Vector2f rotVec = mInput.getDispVec();
        m_camera.movRot(rotVec.x * MOUSE_SENSITIVITY, rotVec.y * MOUSE_SENSITIVITY, 0);


        SceneLight sceneLight = scene.getSceneLight();
        
        // Update directional light direction, intensity and colour
        DirectionalLight directionalLight = sceneLight.getDirectionalLight();
        m_lightAngle += 1.1f;
        if (m_lightAngle > 90) {
            directionalLight.setIntensity(0);
            if (m_lightAngle >= 360) {
                m_lightAngle = -90;
            }
            sceneLight.getAmbientLight().set(0.3f, 0.3f, 0.4f);
        } else if (m_lightAngle <= -80 || m_lightAngle >= 80) {
            float factor = 1 - (float) (Math.abs(m_lightAngle) - 80) / 10.0f;
            sceneLight.getAmbientLight().set(factor, factor, factor);
            directionalLight.setIntensity(factor);
            directionalLight.getColor().y = Math.max(factor, 0.9f);
            directionalLight.getColor().z = Math.max(factor, 0.5f);
        } else {
            sceneLight.getAmbientLight().set(1, 1, 1);
            directionalLight.setIntensity(1);
            directionalLight.getColor().x = 1;
            directionalLight.getColor().y = 1;
            directionalLight.getColor().z = 1;
        }
        double angRad = Math.toRadians(m_lightAngle);
        directionalLight.getDirection().x = (float) Math.sin(angRad);
        directionalLight.getDirection().y = (float) Math.cos(angRad);
        
    }
    
    @Override public void frame5update(){

        //System.out.println("Frame 5 update is called");

        
        if(dialog_update){
            //System.out.println("InputFlag is: " + m_hud.getInputFlag());
            m_hud.updateDialog();
        }
        if(!m_hud.dialogStateFlag){
            dialog_update = false;
        }
        
        if(inputFlag){
            m_hud.setInputFlag(inputFlag);
            inputFlag = false;
        }
        
    }
    
    @Override
    public void render(Window mWindow){
        
        //if (mWindow.isResized()){
            //glViewport(0,0, mWindow.getWidth(), mWindow.getHeight());
          //  mWindow.setResized(false);
        //}
        
        //mWindow.setClearColor(color, color, color, 0.0f);
        renderer.render(mWindow, m_camera, scene, m_hud);
    }
    
    @Override
    public void cleanUp(){
        renderer.cleanup();
        Map<Mesh, List<GameItem>> mapMeshes = scene.getGameMeshes();
        for (Mesh mesh : mapMeshes.keySet()) {
            mesh.cleanUp();
        }
        m_hud.cleanup();
    }
    
    private void setupLights(){
        SceneLight sceneLight = new SceneLight();
        scene.setSceneLight(sceneLight);

        // Ambient Light
        sceneLight.setAmbientLight(new Vector3f(1.0f, 1.0f, 1.0f));

        // Directional Light
        float lightIntensity = 1.0f;
        Vector3f lightPosition = new Vector3f(-1, 0, 0);
        sceneLight.setDirectionalLight(new DirectionalLight(new Vector3f(1, 1, 1), lightPosition, lightIntensity));
    }
    
}
