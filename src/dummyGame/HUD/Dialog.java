/*
 * 
 */
package dummyGame.HUD;

import java.awt.Font;
import java.util.Map;
import oxygenengine.graph.FontTexture;
import oxygenengine.hud.DialogParagraph;
import oxygenengine.hud.HudItem;
import oxygenengine.hud.TextItem;

/**
 *
 * @author zeroxgen
 */
public class Dialog {

    private static final Font FONT = new Font("Arial", Font.PLAIN, 16);
    
    private static final String CHARSET = "ISO-8859-1";
    
    private DialogParagraph m_dp;
    
    private int updateFlag = 0;
    
    private int totalNumLine;
    
    private int currNumLine;
    private int currChar;
    
    private String totalString;
    private String[] stringToken;
    private final Map<String, DialogParagraph> m_map;
    
    private HudItem m_hudItem;
    
    public Dialog(Map<String, DialogParagraph> map, String chainName) throws Exception{
        FontTexture fontTexture = new FontTexture(FONT, CHARSET);

        m_dp = map.get(chainName);
        totalNumLine = m_dp.getNumLine();


        m_map = map;
    
        m_hudItem = new HudItem("dialogHud");
        
        loadHudItem();
        
        currNumLine = 0;
        currChar = 0;
        totalString = m_dp.getLine(0);
        stringToken = totalString.split("");        
    }
    
    public int getUpdateFlag(){
        return updateFlag;
    }
    
    public void setUpdateFlag(int x){
        updateFlag = x;
    }
    
    public void updateString(){
        
        switch(updateFlag){
            
            case 0: //update characters/check if last char
                
                if(currChar != (stringToken.length)){ //if not end of line
                    m_hudItem.getTextItem(currNumLine).setText(m_hudItem.getTextItem(currNumLine).getText() + stringToken[currChar]);  //update current string with character
                    currChar++; //set to next character
                }else{ //if end of line 
                    updateFlag = 1;
                }   
                
                break;
                
            case 1: //end of line, check if last line, else load new line
                    if(currNumLine != (totalNumLine) -1){ //if not last line
                        currNumLine++; //update current line
                        totalString = m_dp.getLine(currNumLine); //get next line, and load it into total string
                        //currString = m_hudItem.getTextItem(currNumLine).getText(); //set currString to be 
                        currChar = 0; //set pointer back to start of string
                        stringToken = totalString.split("");   // split total string into it's chunks
                        updateFlag = 0; //set to be reading from line again
                    }
                    else{ //if last line set updateFlag to be 1 (wait flag)
                        updateFlag = 2; //set to wait for next paragraph to be called
                    }    
                break;
                
            case 2: //wait flag
                //do nothing while waiting
                break;
                
            case 3: //check if last paragraph, else 
                
                if(loadParagraph()){ //if new paragraph sucessfully loaded
                    updateFlag = 0; //load paragraph was success, set back to read chars
                }else{
                    updateFlag = 4; //no more paragraphs, set to wait to close
                }
                
                break;
                
            case 4: //end of chain, wait to clean up
                
                break;
            
                
        }

            
        
        
       // return currString;
    }
    
    public void endWait(){
        if(updateFlag == 2){
            updateFlag = 3;
        } else if(updateFlag == 4){
            updateFlag = 5;
        }
    }
    
    public boolean loadParagraph(){
        
            if(!(m_dp.getNextBlock().equals("null"))){ //if name of next block is not null
                try{
                    m_dp = m_map.get(m_dp.getNextBlock());
                    totalNumLine = m_dp.getNumLine();
                
                    m_hudItem.getTextItems().clear();
                           
                   loadHudItem();
                   currNumLine = 0;
                   currChar = 0;
                   totalString = m_dp.getLine(0);
                   stringToken = totalString.split("");
                   
                } catch(Exception e){
                    System.out.println("Dialogue block not found");
                    return false;
                }
                
                return true;
            }else{
                return false;
            }
    }
    
    
    public HudItem getHudItem(){
        return m_hudItem;
    }
    
    public final void loadHudItem(){

        try{
        
        FontTexture fontTexture = new FontTexture(FONT, CHARSET);         

        for(int c = 0; c < totalNumLine; c++){
            TextItem tItem = new TextItem("", fontTexture);
            tItem.setPos(0, c * 20 + 250, 0);
            m_hudItem.addTextItem(tItem);
            System.out.println(m_hudItem.getTextItem(c).getText() + c);
        }
        }catch(Exception e){
            System.out.println("Error bungo");
        }
    }
    
}
