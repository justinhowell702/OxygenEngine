/*
 * 
 */
package dummyGame.HUD;

//import static com.sun.tools.doclint.HtmlTag.Attr.CHARSET;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.joml.Vector4f;
import oxygenengine.items.GameItem;
import oxygenengine.IHud;
import oxygenengine.Window;
import oxygenengine.graph.FontTexture;
import oxygenengine.graph.Material;
import oxygenengine.graph.Mesh;
import oxygenengine.graph.OBJloader;
import oxygenengine.hud.DialogParagraph;
import oxygenengine.hud.DialogReader;
import oxygenengine.hud.HudItem;
import oxygenengine.hud.TextItem;

/**
 *
 * @author zeroxgen
 */
public class Hud implements IHud {
    
    private final GameItem[] m_gItems;
    
    private Dialog m_dialog;
    
    public boolean dialogStateFlag = false;
    
    private List<HudItem> m_renderStack = new ArrayList<>();
    
    private final TextItem[] m_tItems;
    
    private final TextItem m_statusTextItem;
    
    private final GameItem m_compass;
    
    private static final Font FONT = new Font("Arial", Font.PLAIN, 20);
    
    private static final String CHARSET = "ISO-8859-1";
    
    private static final String FONT_TEXTURE = "textures/font.png";
    
    private final Window mWindow;
    
    public Map<String, DialogParagraph> m_map;

    private GameItem m_dialogBox;
    
    private boolean inputFlag = false;
    
    public Hud(Window mWindow, String statusText) throws Exception{
        this.mWindow = mWindow;
        FontTexture fontTexture = new FontTexture(FONT, CHARSET);
        
        this.m_statusTextItem = new TextItem(statusText, fontTexture);
        this.m_statusTextItem.getMesh().getMaterial().setAmbientColor(new Vector4f(1, 1, 1, 1));
        
        
        //create compass
        Mesh mesh = OBJloader.loadMesh("/resources/models/compass.obj");
        Material material = new Material();
        material.setAmbientColor(new Vector4f(1, 0, 0, 1));
        mesh.setMaterial(material);
        m_compass = new GameItem(mesh);
        m_compass.setScale(40.0f);
        //rotate to transform it to screen
        m_compass.setRot(0f, 0f, 180f);
        
        HudItem tempHItem = new HudItem("test_item",m_statusTextItem, m_compass);
        
        m_renderStack.add(tempHItem);
        
        m_tItems = new TextItem[]{m_statusTextItem};
        m_gItems = new GameItem[]{m_compass};
        
        
        m_statusTextItem.setPos(50, 50, 0);
        m_compass.setPos(50, 50, 0);

        loadDialogMap("/dummyGame/resources/dialog/test_dialog.dia");
        
        //startDialog("test_dialog_0");
        
    }
    
    public boolean getInputFlag(){
        return inputFlag;
    }
    
    public void setInputFlag(boolean x){
        inputFlag = x;
    }
    
    public void setStatusText(String statusText){
        m_statusTextItem.setText(statusText);
    }
    
    @Override
    public GameItem[] getGameItems(){
        return m_gItems;
    }
    
    @Override
    public TextItem[] getTextItems(){
        return m_tItems;
    }
    
    @Override
    public List<HudItem> getRenderStack(){
        return m_renderStack;
    }
    
    public void rotateCompass(float angle){
        this.m_compass.setRot(0, 0, 180 + angle);
    }
    
    public void updateSize(Window mWindow) {
        //this.m_statusTextItem.setPosition(10f, (float)(mWindow.getWindowHeight()) - 50f, 0);
    }
    
    
    public void loadDialogMap(String fileName) throws Exception{
        DialogReader dr = new DialogReader();
        m_map = dr.readDiaFile(fileName);
    }
    
    public void startDialog(String startString) throws Exception{
        m_dialog = new Dialog(m_map, startString);
        m_renderStack.add(m_dialog.getHudItem());
        System.out.println("Start dialog called");
        inputFlag = false;
        dialogStateFlag = true;
        //System.out.println("Input flag status is: " + inputFlag);
        //m_renderStack.get(dialogStateFlag)
    }
    
    public void updateDialog(){
        if(m_dialog.getUpdateFlag() == 2 && inputFlag){ //if end of line, and button is pressed
            inputFlag = false;
            System.out.println("set to false" + inputFlag);
            m_dialog.setUpdateFlag(3); //load next paragraph
        }else if(m_dialog.getUpdateFlag() == 4 && inputFlag){ //if update flag is set to 4, and button is pressed
            inputFlag = false;
            endDialog();
        } 
        
        else { // updateString regardless, 2 and 4 will do nothing
            m_dialog.updateString();
            inputFlag = false;

        }
    }
    
    public void endDialog(){
        m_dialog.getHudItem().getTextItems().clear();
        //m_dialog.getHudItem().getGameItems().clear();
        
        int sizeStack = m_renderStack.size();
        for(int i = 0; i < sizeStack; i++){ //find dialogitem on stack
            if(m_renderStack.get(i).getID() == "dialogHUD"){
                m_renderStack.remove(i);
            }
        }
        
        dialogStateFlag = false;
    }
    
}
