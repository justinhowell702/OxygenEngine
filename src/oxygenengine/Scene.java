/*
 * 
 */
package oxygenengine;

import oxygenengine.items.SkyBox;
import oxygenengine.items.GameItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import oxygenengine.graph.Mesh;
import oxygenengine.graph.lighting.SceneLight;

/**
 *
 * @author zeroxgen
 */
public class Scene {
    
    
    private SkyBox skybox;
    
    private SceneLight sceneLight;
    
    private Map<Mesh, List<GameItem>> meshMap;
    
    public Map<Mesh, List<GameItem>> getGameMeshes(){
        return meshMap;
    }
    
    public Scene(){
        meshMap = new HashMap();
    }
    
    public void setGameItems(GameItem[] gItems){
        int numGameItems = gItems != null ? gItems.length : 0;
        for(int i = 0; i < numGameItems; i++){
            GameItem gItem = gItems[i];
            Mesh mesh = gItem.getMesh();
            List<GameItem> list = meshMap.get(mesh);
            if(list == null){
                list = new ArrayList<>();
                meshMap.put(mesh, list);
            }
            list.add(gItem);
        }
    }
    
    public SkyBox getSkyBox(){
        return skybox;
    }
    
    public void setSkyBox(SkyBox skybox){
        this.skybox = skybox;
    }
    
    public SceneLight getSceneLight(){
        return sceneLight;
    }
    
    public void setSceneLight(SceneLight sLight){
        sceneLight = sLight;
    }
    
}
