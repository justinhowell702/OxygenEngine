/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxygenengine.hud;

import java.util.ArrayList;
import java.util.List;
import oxygenengine.items.GameItem;

/**
 *
 * @author JustinHowell
 */
public class HudItem {
    
    private String m_id;
    
    private static final float ZPOS = 0.0f;
    
    private List<TextItem> m_textItems = new ArrayList<>();
    
    private List<GameItem> m_gameItems = new ArrayList<>();
    
    public HudItem(String id){
        m_id = id;
    }
    
    public HudItem(String id, TextItem textItem){
        m_id = id;
        m_textItems.add(textItem);
    }
    
    public HudItem(String id, GameItem gItem){
        m_id = id;
        m_gameItems.add(gItem);
    }
    
    public HudItem(String id, TextItem tItem, GameItem gItem){
        m_id = id;
        m_textItems.add(tItem);
        m_gameItems.add(gItem);        
    }
    
    public HudItem(String id, TextItem[] textItems){
        m_id = id;
        for(int c = 0; c < textItems.length ; c++){
            m_textItems.add(textItems[c]);
        }        
    }
    
    public HudItem(String id, TextItem[] textItems, GameItem[] gameItems){
        m_id = id;
        for(int c = 0; c < textItems.length ; c++){
            m_textItems.add(textItems[c]);
        }
        for(int c = 0; c < gameItems.length ; c++){
            m_gameItems.add(gameItems[c]);
        }
    }
    
    public HudItem(String id, GameItem[] gItems){
        m_id = id;
        for(int c = 0; c < gItems.length ; c++){
            m_gameItems.add(gItems[c]);
        }
    }
    
    public List<GameItem> getGameItems(){
        return m_gameItems;
    }
    
    public List<TextItem> getTextItems(){
        return m_textItems;
    }
    
    public GameItem getGameItem(int reference){
        return m_gameItems.get(reference);
    }
    
    public TextItem getTextItem(int reference){
        return m_textItems.get(reference);
    }
    
    public void addTextItem(TextItem tItem){
        m_textItems.add(tItem);
    }
    
    public void addGameItem(GameItem gItem){
        m_gameItems.add(gItem);
    }
    
    public String getID(){
        return m_id;
    }
    
    public void setID(String id){
        m_id = id;
    }
    
}
