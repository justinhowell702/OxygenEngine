/*
 * 
 */
package oxygenengine.hud;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zeroxgen
 */
public class DialogParagraph {
        List<String> m_lines = new ArrayList<>(); //actual paragraph lines
        List<String> m_portraitFiles = new ArrayList<>(); //file names of portrait data to be loaded
                
        String m_nextParagraph; //name pointer to next paragraph
        int numLine = 0; //number of lines per 
        int m_focus = 0; //0 is no focus/object, 1 is left, 2 is right
                
        public DialogParagraph(){
            m_portraitFiles = null;
        };
        public String getLine(int stringNum){
            return m_lines.get(stringNum);
        }
        public String getNextBlock(){
            return m_nextParagraph;
        }
        
        public int getNumLine(){
            return numLine;
        }
}
