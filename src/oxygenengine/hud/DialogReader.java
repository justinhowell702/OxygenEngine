/*
 * 
 */
package oxygenengine.hud;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import oxygenengine.Utility;

/**
 *
 * @author zeroxgen
 */
public class DialogReader {
    
    public Map<String, DialogParagraph> m_map = new HashMap<>();
        
    public Map<String, DialogParagraph> readDiaFile(String fileName) throws Exception{
        List<String> list = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(Class.forName(Utility.class.getName()).getResourceAsStream(fileName)))) {        
        
            String line ;
            while ((line = br.readLine()) != null){
                list.add(line);
            }
            
        }
        
        
        for(int i = 0; i < list.size() ; i++){
            
            DialogParagraph dp = new DialogParagraph();
            String line = list.get(i);
            String[] token = line.split("#");
            
            
            dp.numLine = Integer.parseInt(token[2]);
            dp.m_nextParagraph = token[4]; //set next paragraph block
            dp.m_focus = Integer.parseInt(token[3]); //set focus, 0 is none, 1 is left, 2 is right
            
            if(token[0].equals("sb")){
                    i++;
                    dp.m_portraitFiles = new ArrayList<>();
                    line = list.get(i);
                    String[] sbToken = line.split("#"); //all character portraits of dialog chain to be loaded
                    for(int c = 0; c < sbToken.length; c++){
                            dp.m_portraitFiles.add(sbToken[c]); //add all to portrait files, loaded when dialog starts
                    }
            }
            
            i++;
            for(int c = 0; c < dp.numLine; c++, i++){
                line = list.get(i);
                dp.m_lines.add(line);
            }
            
            m_map.put(token[1], dp);            
            //i++;
            
        }
        
            
        
            return m_map;
    }
    
}
