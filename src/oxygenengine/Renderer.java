/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxygenengine;

import oxygenengine.items.SkyBox;
import oxygenengine.items.GameItem;
import java.util.List;
import java.util.Map;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import oxygenengine.graph.Shader;

import static org.lwjgl.opengl.GL11.*;
import oxygenengine.graph.Camera;
import oxygenengine.graph.Mesh;
import oxygenengine.graph.Transformations;
import oxygenengine.graph.lighting.DirectionalLight;
import oxygenengine.graph.lighting.PointLight;
import oxygenengine.graph.lighting.SceneLight;
import oxygenengine.graph.lighting.SpotLight;
import oxygenengine.hud.HudItem;

/**
 *
 * @author zeroxgen
 */
public class Renderer {
    
    private Shader m_sceneShader;
    private Shader m_hudShader;
    private Shader m_skyboxShader;
    
    private static final float m_FOV = (float) Math.toRadians(60.0f);
    private static final float m_ZNEAR = 0.01f;
    private static final float m_ZFAR = 1000.0f;
    
    private static final int MAX_POINT_LIGHTS = 5;
    private static final int MAX_SPOT_LIGHTS = 5;
    
    private float m_specularPower;
    
    private Transformations transf;
    
    public Renderer() {
        transf = new Transformations();
        m_specularPower = 10f;
    }

    public void init(Window mWindow) throws Exception {
        setSceneShader();
        setHUDshader();
        setSkyboxShader();
    }    
    
    public void setSceneShader() throws Exception{
        m_sceneShader = new Shader();
        m_sceneShader.createVertexShader(Utility.loadResource("/resources/shader/vertex.vs"));
        m_sceneShader.createFragShader(Utility.loadResource("/resources/shader/fragment.fs"));
        
        m_sceneShader.link();
        m_sceneShader.createUniform("projectionMatrix");
        m_sceneShader.createUniform("modelViewMatrix");
        m_sceneShader.createUniform("texture_sampler");
        
        m_sceneShader.createMaterialUniform("material");
        
        m_sceneShader.createUniform("specularPower");
        m_sceneShader.createUniform("ambientLight");
        m_sceneShader.createPointLightListUniform("pointLights", MAX_POINT_LIGHTS);
        m_sceneShader.createSpotLightListUniform("spotLights", MAX_SPOT_LIGHTS);
        m_sceneShader.createDirectionalLightUniform("directionalLight");
    }
    
    public void setHUDshader() throws Exception {
        m_hudShader = new Shader();
        m_hudShader.createVertexShader(Utility.loadResource("/resources/shader/hud_vertex.vs"));
        m_hudShader.createFragShader(Utility.loadResource("/resources/shader/hud_fragment.fs"));
        
        m_hudShader.link();
        
        m_hudShader.createUniform("projModelMatrix");
        m_hudShader.createUniform("color");
        m_hudShader.createUniform("hasTexture");
        
    }
    
    public void setSkyboxShader() throws Exception{
        m_skyboxShader = new Shader();
        m_skyboxShader.createVertexShader(Utility.loadResource("/resources/shader/sb_vertex.vs"));
        m_skyboxShader.createFragShader(Utility.loadResource("/resources/shader/sb_fragment.fs"));
        m_skyboxShader.link();
        
        //create uniforms for projection matrix
        m_skyboxShader.createUniform("projectionMatrix");
        m_skyboxShader.createUniform("modelViewMatrix");
        m_skyboxShader.createUniform("texture_sampler");
        m_skyboxShader.createUniform("ambientLight");
        
    }
    
    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    public void render(Window mWindow, Camera camera, Scene scene, IHud hud) {
        clear();

        transf.updateProjectionMatrix(m_FOV, mWindow.getWindowWidth(), mWindow.getWindowHeight(), m_ZNEAR, m_ZFAR);
        transf.updateViewMatrix(camera);        
        
        renderScene(mWindow, camera, scene);
        
        renderSkyBox(mWindow, camera, scene);
        
        renderHud(mWindow, hud);
        
    }

    public void renderScene(Window mWindow, Camera camera, Scene scene){
        m_sceneShader.bind();

        //update projection matrix
        Matrix4f projectionMatrix = transf.getProjectionMatrix();
        m_sceneShader.setUniform("projectionMatrix", projectionMatrix);
        
        //update view matrix
        Matrix4f viewMatrix = transf.getViewMatrix();

        SceneLight sceneLight = scene.getSceneLight();
        renderLights(viewMatrix, sceneLight);
        
        m_sceneShader.setUniform("texture_sampler", 0);

        Map<Mesh, List<GameItem>> mapMeshes = scene.getGameMeshes();
        
        for(Mesh mesh : mapMeshes.keySet()){

            m_sceneShader.setUniform("material", mesh.getMaterial());
        
            mesh.renderList(mapMeshes.get(mesh), (GameItem gameItem) -> {
            Matrix4f modelViewMatrix = transf.buildModelViewMatrix(gameItem, viewMatrix);
            m_sceneShader.setUniform("modelViewMatrix", modelViewMatrix);
            }
            );
        }        
        
        m_sceneShader.unbind();
        
    }
    
    public void renderSkyBox(Window mWindow, Camera camera, Scene scene) {
        m_skyboxShader.bind();
        
        m_skyboxShader.setUniform("texture_sampler", 0);
        
        //update projection matrix
        Matrix4f projectionMatrix = transf.getProjectionMatrix();
        m_skyboxShader.setUniform("projectionMatrix", projectionMatrix);
        
        SkyBox skybox = scene.getSkyBox();
        
        Matrix4f viewMatrix = transf.getViewMatrix();
        
        viewMatrix.m30(0);
        viewMatrix.m31(0);
        viewMatrix.m32(0);
        
        Matrix4f modelViewMatrix = transf.buildModelViewMatrix(skybox, viewMatrix);
        
        m_skyboxShader.setUniform("modelViewMatrix", modelViewMatrix);
        
        m_skyboxShader.setUniform("ambientLight", scene.getSceneLight().getAmbientLight());
        
        scene.getSkyBox().getMesh().render();
        
        m_skyboxShader.unbind();
        
    }
    
    public void renderHud(Window mWindow, IHud hud){
        
        glDisable(GL_DEPTH_TEST);
        
        m_hudShader.bind();
        
        Matrix4f ortho = transf.getOrthoProjectionMatrix(0, mWindow.getWindowWidth(), mWindow.getWindowHeight(), 0);
        
        for(HudItem hItem : hud.getRenderStack()){
            for(GameItem gItem : hItem.getGameItems()){
                Mesh mesh = gItem.getMesh();
                //set ortho and model matrix for HUD item
                Matrix4f projModelMatrix = transf.buildOrtoProjModelMatrix(gItem, ortho);
                m_hudShader.setUniform("projModelMatrix", projModelMatrix);
                m_hudShader.setUniform("color", gItem.getMesh().getMaterial().getAmbientColor());
                m_hudShader.setUniform("hasTexture", gItem.getMesh().getMaterial().isTextured() ? 1 : 0);
            
                mesh.render();                
            }
            for(GameItem tItem : hItem.getTextItems()){
                Mesh mesh = tItem.getMesh();
                //set ortho and model matrix for HUD item
                Matrix4f projModelMatrix = transf.buildOrtoProjModelMatrix(tItem, ortho);
                m_hudShader.setUniform("projModelMatrix", projModelMatrix);
                m_hudShader.setUniform("color", tItem.getMesh().getMaterial().getAmbientColor());
                m_hudShader.setUniform("hasTexture", tItem.getMesh().getMaterial().isTextured() ? 1 : 0);
            
                mesh.render();            
            }            
        }
        
        

        
        m_hudShader.unbind();
        
        glEnable(GL_DEPTH_TEST);
    }
    
    public void cleanup() {
        if (m_sceneShader != null) {
            m_sceneShader.cleanUp();
        }
        if (m_hudShader != null) {
            m_hudShader.cleanUp();
        }
        if (m_skyboxShader != null){
            m_skyboxShader.cleanUp();
        }
    }
    
    public void renderLights(Matrix4f viewMatrix, SceneLight sceneLights){
        
        m_sceneShader.setUniform("ambientLight", sceneLights.getAmbientLight());
        m_sceneShader.setUniform("specularPower", m_specularPower);
        
        //process point light
        PointLight[] pLightList = sceneLights.getPointLightList();
        int numLights = pLightList != null ? pLightList.length : 0;
        for ( int i = 0; i < numLights; i++){
            PointLight currPointLight = new PointLight(pLightList[i]);
            Vector3f lightPos = currPointLight.getPosition();
            Vector4f aux = new Vector4f(lightPos, 1);
            aux.mul(viewMatrix);
            lightPos.x = aux.x;
            lightPos.y = aux.y;
            lightPos.z = aux.z;
            m_sceneShader.setUniform("pointLights", currPointLight, i);
        }
        
        //process spot lights
        SpotLight[] sLightList = sceneLights.getSpotLightList();
        numLights = sLightList != null ? sLightList.length : 0;
        for (int i = 0; i < numLights ; i++){
            SpotLight currSpotLight = new SpotLight(sLightList[i]);
            Vector4f dir = new Vector4f(currSpotLight.getConeDirection(), 0);
            dir.mul(viewMatrix);
            currSpotLight.setConeDirection(new Vector3f(dir.x, dir.y, dir.z));
            Vector3f lightPos = currSpotLight.getPointLight().getPosition();

            Vector4f aux = new Vector4f(lightPos, 1);
            aux.mul(viewMatrix);

            lightPos.x = aux.x;
            lightPos.y = aux.y;
            lightPos.z = aux.z;

            m_sceneShader.setUniform("spotLights", currSpotLight, i);
        }
        
        //copy of dLight
        DirectionalLight currDirLight = new DirectionalLight(sceneLights.getDirectionalLight());
        Vector4f dir = new Vector4f(currDirLight.getDirection(), 0);
        dir.mul(viewMatrix);
        currDirLight.setDirection(new Vector3f(dir.x, dir.y, dir.z));
        m_sceneShader.setUniform("directionalLight", currDirLight);
        
    }
    
}
