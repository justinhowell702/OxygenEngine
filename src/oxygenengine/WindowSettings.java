
package oxygenengine;

/**
 *
 * @author zeroxgen
 */
public class WindowSettings {
    
    int m_height;
    int m_width;
    String m_title;
    
    public WindowSettings(){
        m_height = 300;
        m_width = 500;
        m_title = "DemoGame";
    };
    
    public WindowSettings(int height, int width, String title){
        m_height = height;
        m_width = width;
        m_title = title;
    };
    
    public int getHeight(){return m_height;};
    
    public int getWidth(){return m_width;};
    
    public String getTitle(){return m_title;};
}
