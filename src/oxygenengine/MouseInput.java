/*
 * 
 */
package oxygenengine;

import org.joml.Vector2d;
import org.joml.Vector2f;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_1;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_2;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.glfwSetCursorEnterCallback;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;

/**
 *
 * @author zeroxgen
 */
public class MouseInput {
    
    private final Vector2d m_prevPos;
    
    private final Vector2d m_currPos;
    
    private final Vector2f m_dispVec;
    
    private boolean inWindow = false;
    
    private boolean leftButtonPressed = false;
    
    private boolean rightButtonPressed = false;
    
    
    public MouseInput(){
        m_prevPos = new Vector2d(-1, -1);
        m_currPos = new Vector2d(0,0);
        m_dispVec = new Vector2f();
    }
    
    public void init(Window mWindow){
        glfwSetCursorPosCallback(mWindow.getWindowHandle(), (windowHandle, posX, posY) -> {
           m_currPos.x = posX;
           m_currPos.y = posY;
        });
        
        glfwSetCursorEnterCallback(mWindow.getWindowHandle(), (windowHandle, entered) ->{
            inWindow = entered;
        });
        
        glfwSetMouseButtonCallback(mWindow.getWindowHandle(), (windowHandle, button, action, mode) -> {
           leftButtonPressed = button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS;
           rightButtonPressed = button == GLFW_MOUSE_BUTTON_2 && action == GLFW_PRESS; 
        });
    }
    
    public Vector2f getDispVec(){
        return m_dispVec;
    }
    
    public void input(Window mWindow){
        m_dispVec.x = 0;
        m_dispVec.y = 0;
        
        if( m_prevPos.x > 0 && m_prevPos.y > 0 && inWindow){
            double changeX = m_currPos.x - m_prevPos.x;
            double changeY = m_currPos.y - m_prevPos.y;
            
            boolean rotX = changeX != 0;
            boolean rotY = changeY != 0;
            
            if(rotX){
                m_dispVec.y = (float) changeX;
            }
            if(rotY){
                m_dispVec.x = (float) changeY;
            }
        }
        
        m_prevPos.x = m_currPos.x;
        m_prevPos.y = m_currPos.y;
        
    }
    
    public boolean isLeftButtonPressed(){
        return leftButtonPressed;
    }
    
    public boolean isRightButtonPressed(){
        return rightButtonPressed;
    }
}
