
package oxygenengine;

/**
 *
 * @author zeroxgen
 */
public interface IGameLogic {

     void init(Window mWindow) throws Exception;

    void frame5update();
     
    void input(Window mWindow, MouseInput mInput);

    void update(MouseInput mInput);
    
    void render(Window mWindow);
    
    void cleanUp();
    
}
