/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxygenengine;

/**
 *
 * @author zeroxgen
 */
public class Time {
    
    private double lastTime;
    public static final long SECOND = 1000000000;
    private static double delta;
    
    
    public static long getTime(){
        return System.nanoTime();
    }
    
    public static double getDelta(){
        return delta;
    }
    
    public static void setDelta(double delta){
        Time.delta = delta;
    }
    public double getLastTime(){
        return lastTime;
    }
    
    public double elapsedTime(){
        double time = getTime();
        double eTime = time - lastTime;
        lastTime = time;
        return lastTime;
    }
    
}
