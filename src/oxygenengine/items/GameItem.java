/*
 * 
 */
package oxygenengine.items;

import org.joml.Vector3f;
import oxygenengine.graph.Mesh;

/**
 *
 * @author zeroxgen
 */
public class GameItem {
    
    private Mesh m_mesh;
    
    private final Vector3f m_position;
    
    private float m_scale;
    
    private final Vector3f m_rotation;
    
    public GameItem(){
        m_position = new Vector3f(0, 0, 0);
        m_scale = 1;
        m_rotation = new Vector3f(0, 0, 0);
    }
    
    public GameItem(Mesh mesh){
        this.m_mesh = mesh;
        m_position = new Vector3f(0,0,0);
        m_scale = 1;
        m_rotation = new Vector3f(0,0,0);
    }
    
    public Vector3f getPos(){
        return m_position;
    }
    
    public void setPos(float x, float y, float z){
        this.m_position.x = x;
        this.m_position.y = y;
        this.m_position.z = z;
    }
    
    public float getScale(){
        return m_scale;
    }
    
    public void setScale(float scale){
        m_scale = scale;
    }
    
    public Vector3f getRot(){
        return m_rotation;
    }
    
    public void setRot(float x, float y, float z){
        this.m_rotation.x = x;
        this.m_rotation.y = y;
        this.m_rotation.z = z;
    }
    
    public Mesh getMesh(){
        return m_mesh;
    }
    
    public void setMesh(Mesh mesh){
        this.m_mesh = mesh;
    }
}
