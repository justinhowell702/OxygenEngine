/*
 * 
 */
package oxygenengine.items;

import oxygenengine.items.GameItem;
import oxygenengine.graph.Material;
import oxygenengine.graph.Mesh;
import oxygenengine.graph.OBJloader;
import oxygenengine.graph.Texture;

/**
 *
 * @author zeroxgen
 */
public class SkyBox extends GameItem {
    
    public SkyBox(String objModel, String textureFile) throws Exception{
        super();
        Mesh skyBoxMesh = OBJloader.loadMesh(objModel);
        Texture skyBoxtexture = new Texture(textureFile);
        skyBoxMesh.setMaterial(new Material(skyBoxtexture, 0.0f));
        setMesh(skyBoxMesh);
        setPos(0, 0, 0);
    }
    
}
