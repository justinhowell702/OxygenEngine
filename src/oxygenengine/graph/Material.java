/*
 * 
 */
package oxygenengine.graph;

import org.joml.Vector4f;

/**
 *
 * @author zeroxgen
 */
public class Material {
    
    private static final Vector4f DEFAULT_COLOR = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
    
    private Vector4f m_ambientColor;
    
    private Vector4f m_diffuseColor;
    
    private Vector4f m_specularColor;
    
    private float m_reflectance;
    
    private Texture m_texture;
    
    public Material(){
        m_ambientColor = DEFAULT_COLOR;
        m_diffuseColor = DEFAULT_COLOR;
        m_specularColor = DEFAULT_COLOR;
        m_texture = null;
        m_reflectance = 0;
    }
    
    public Material(Vector4f colour, float reflectance) {
        this(colour, colour, colour, null, reflectance);
    }
    
    public Material(Texture texture){
        this(DEFAULT_COLOR, DEFAULT_COLOR, DEFAULT_COLOR, texture, 0);
    }
    
    public Material(Texture texture, float reflectance){
        this(DEFAULT_COLOR, DEFAULT_COLOR, DEFAULT_COLOR, texture, reflectance);
    }
    
    public Material(Vector4f ambient, Vector4f diffuse, Vector4f specular, Texture texture, float refl){
        m_ambientColor = ambient;
        m_diffuseColor = diffuse;
        m_specularColor = specular;
        m_texture = texture;
        m_reflectance = refl;
    }
    
    public Vector4f getAmbientColor(){
        return m_ambientColor;
    }
    
    public void setAmbientColor(Vector4f ambient){
        m_ambientColor = ambient;
    }
    
    public Vector4f getDiffuseColor(){
        return m_diffuseColor;
    }
    
    public void setDiffuseColor(Vector4f diffuse){
        m_diffuseColor = diffuse;
    }
    
    public Vector4f getSpecularColor(){
        return m_specularColor;
    }
    
    public void setSpecularColor(Vector4f specular){
        m_specularColor = specular;
    }
    
    public float getReflectance(){
        return m_reflectance;
    }
    
    public void setReflectance(float refl){
        m_reflectance = refl;
    }
    
    public boolean isTextured(){
        return m_texture != null;
    }
    
    public Texture getTexture(){
        return m_texture;
    }
    
    public void setTexture(Texture texture){
        m_texture = texture;
    }
}
