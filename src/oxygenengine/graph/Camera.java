/*
 * 
 */
package oxygenengine.graph;

import org.joml.Vector3f;

/**
 *
 * @author zeroxgen
 */
public class Camera {
    
    private final Vector3f m_position;
    
    private final Vector3f m_rotation;
    
    public Camera(){
        m_position = new Vector3f(0,0,0);
        m_rotation = new Vector3f(0,0,0);   
    }
    
    public Camera(Vector3f pos, Vector3f rot){
        m_position = pos;
        m_rotation = rot;
    }
    
    public Vector3f getPos(){
        return m_position;
    }
    
    public void setPos(float x, float y, float z){
        m_position.x = x;
        m_position.y = y;
        m_position.z = z;
    }
    
    public void movePos(float offsetX, float offsetY, float offsetZ){
        if ( offsetZ != 0){
            m_position.x += (float)Math.sin(Math.toRadians(m_rotation.y)) * -1.0f * offsetZ;
            m_position.z += (float)Math.cos(Math.toRadians(m_rotation.y)) * offsetZ;
        }
        
        if ( offsetX != 0){
            m_position.x += (float)Math.sin(Math.toRadians(m_rotation.y - 90)) * -1.0f * offsetX;
            m_position.z += (float)Math.cos(Math.toRadians(m_rotation.y - 90)) * offsetX;
        }
    
        m_position.y += offsetY;
    }
    
    public Vector3f getRot(){
        return m_rotation;
    }
    
    public void setRot(float rotX, float rotY, float rotZ){
        m_rotation.x = rotX;
        m_rotation.y = rotY;
        m_rotation.z = rotZ;
    }
    
    public void movRot(float offsetX, float offsetY, float offsetZ){
        m_rotation.x += offsetX;
        m_rotation.y += offsetY;
        m_rotation.z += offsetZ;
    }
}
