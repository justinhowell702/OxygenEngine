/*
 * 
 */
package oxygenengine.graph;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_UNPACK_ALIGNMENT;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glPixelStorei;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;
import static org.lwjgl.stb.STBImage.stbi_failure_reason;
import static org.lwjgl.stb.STBImage.stbi_image_free;
import static org.lwjgl.stb.STBImage.stbi_load;
import static org.lwjgl.stb.STBImage.stbi_load_from_memory;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author zeroxgen
 */
public class Texture {
    
    private final int m_textureID;
    
    private int m_width;
    
    private int m_height;
    
    public Texture( String fileName ) throws Exception{
        
    ByteBuffer buf;
        // Load Texture file
        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer w = stack.mallocInt(1);
            IntBuffer h = stack.mallocInt(1);
            IntBuffer channels = stack.mallocInt(1);

            buf = stbi_load(fileName, w, h, channels, 4);
            if (buf == null) {
                throw new Exception("Image file [" + fileName  + "] not loaded: " + stbi_failure_reason());
            }

            m_width = w.get();
            m_height = h.get();
        }

        
        //create OpenGL texture
        int textureID = glGenTextures();
        
        //bind texture
        glBindTexture(GL_TEXTURE_2D, textureID);
        
        //how to unpack RGBA bytes, each component is 1 byte
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        
        //upload texture data
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA,
                                        GL_UNSIGNED_BYTE, buf);
        
        //generate mip map
        glGenerateMipmap(GL_TEXTURE_2D);
        
        stbi_image_free(buf);
        
        m_textureID = textureID;        
        
    }
    
    public Texture(ByteBuffer imageBuffer) throws Exception{
        ByteBuffer buf;
        //load texture file
        try ( MemoryStack stack = MemoryStack.stackPush()){
            IntBuffer w = stack.mallocInt(1);
            IntBuffer h = stack.mallocInt(1);
            IntBuffer channels = stack.mallocInt(1);

            buf = stbi_load_from_memory(imageBuffer, w, h, channels, 4);
            if (buf == null) {
                throw new Exception("Image file not loaded: " + stbi_failure_reason());
            }

            m_width = w.get();
            m_height = h.get();            
        }
        
        m_textureID = createTexture(buf);
        stbi_image_free(buf);
    }
    
    private int createTexture(ByteBuffer buf){
        //create opengl texture
        int textureId = glGenTextures();
        //bind texture
        glBindTexture(GL_TEXTURE_2D, textureId);
        
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buf);
        
        glGenerateMipmap(GL_TEXTURE_2D);
        
        return textureId;
        
    }
    
    public Texture(int id){
        m_textureID = id;
    }
    
    public int getID(){
        return m_textureID;
    }
    

    
    public int getWidth(){
        return m_width;
    }
    
    public int getHeight(){
      return m_height;
    }
    
    public void cleanUp(){
        glDeleteTextures(m_textureID);
    }
}
