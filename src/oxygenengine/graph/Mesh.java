/*
 * 
 */
package oxygenengine.graph;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import org.lwjgl.system.MemoryUtil;
import static org.lwjgl.opengl.GL30.*;
import oxygenengine.items.GameItem;

/**
 *
 * @author zeroxgen
 */
public class Mesh {
    
    private final int m_vaoID;
    
    private final int m_numVertex;
        
    private Material m_material;
    
    private final List<Integer> m_vboIDlist;
    
    public Mesh(float[] vertexPositions, float[] textureCoords, float[] normals, int[] indices){//float[] color){
        
        FloatBuffer posBuffer = null;
        FloatBuffer textureCoordBuffer = null;
        IntBuffer indexBuffer = null;
        FloatBuffer vectorNormalBuffer = null;
        
        try{
            m_numVertex = indices.length;
            m_vboIDlist = new ArrayList();
            
            m_vaoID = glGenVertexArrays();
            glBindVertexArray(m_vaoID);
            

            //position/vertex VBO
            
            int vboID = glGenBuffers();
            m_vboIDlist.add(vboID);
            posBuffer = MemoryUtil.memAllocFloat(vertexPositions.length);
            posBuffer.put(vertexPositions).flip();
            glBindBuffer(GL_ARRAY_BUFFER, vboID);
            glBufferData(GL_ARRAY_BUFFER, posBuffer, GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
            
            //texture coords vbo
            vboID = glGenBuffers();
            m_vboIDlist.add(vboID);
            textureCoordBuffer = MemoryUtil.memAllocFloat(textureCoords.length);
            textureCoordBuffer.put(textureCoords).flip();
            glBindBuffer(GL_ARRAY_BUFFER, vboID);
            glBufferData(GL_ARRAY_BUFFER, textureCoordBuffer, GL_STATIC_DRAW);
            glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);         
            
            //vertex normals VBO
            vboID = glGenBuffers();
            m_vboIDlist.add(vboID);
            vectorNormalBuffer = MemoryUtil.memAllocFloat(normals.length);
            vectorNormalBuffer.put(normals).flip();
            glBindBuffer(GL_ARRAY_BUFFER, vboID);
            glBufferData(GL_ARRAY_BUFFER, vectorNormalBuffer, GL_STATIC_DRAW);
            glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);
            
           
            //indices VBO
            
            vboID = glGenBuffers();
            m_vboIDlist.add(vboID);
            indexBuffer = MemoryUtil.memAllocInt(indices.length);
            indexBuffer.put(indices).flip();
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexBuffer, GL_STATIC_DRAW);
            
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
            
        } finally {
            if(posBuffer != null){
                MemoryUtil.memFree(posBuffer);
            }
            if(indexBuffer != null){
                MemoryUtil.memFree(indexBuffer);
            }
            if(vectorNormalBuffer != null){
                MemoryUtil.memFree(vectorNormalBuffer);
            }
            if(textureCoordBuffer != null){
                MemoryUtil.memFree(textureCoordBuffer);
            }
        }
        
    }
    
    
    public Material getMaterial(){
        return m_material;
    }
    
    public void setMaterial(Material material){
        m_material = material;
    }
    
    
    public int getVAOID(){
        return m_vaoID;
    }
    
    public int getNumVertex(){
        return m_numVertex;
    }
    
    public void cleanUp(){
        glDisableVertexAttribArray(0);
        
        //delete both vbos
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        for (int vboID : m_vboIDlist){
            glDeleteBuffers(vboID);
        }
        
        Texture texture = m_material.getTexture();
        
        if(texture != null){
            texture.cleanUp();
        }
        
        //delete vao
        glBindVertexArray(0);
        glDeleteVertexArrays(m_vaoID);
        
    }
    
    public void deleteBuffers(){
        glDisableVertexAttribArray(0);
        
        //delete vbo
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        for(int vboID : m_vboIDlist) {
            glDeleteBuffers(vboID);
        }
        
        //delete the VAO
        glBindVertexArray(0);
        glDeleteVertexArrays(m_vaoID);
        
    }
    
    public void initRender(){
        Texture texture = m_material.getTexture();
        if(texture != null){
            //activate texture bank
            glActiveTexture(GL_TEXTURE0);
            //bind texture
            glBindTexture(GL_TEXTURE_2D, texture.getID());
        }
                // Bind to the VAO
        glBindVertexArray(getVAOID());
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
    }
    
    public void endRender(){
        
        // Restore state
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    
    public void render(){
        initRender();
        // Draw the vertices
        glDrawElements(GL_TRIANGLES, getNumVertex(), GL_UNSIGNED_INT, 0);
        endRender();
    }

    public void renderList(List<GameItem> gItems, Consumer<GameItem> consumer){
    initRender();

    for (GameItem gameItem : gItems) {
        // Set up data required by gameItem
        consumer.accept(gameItem);
        // Render this game item
        glDrawElements(GL_TRIANGLES, getNumVertex(), GL_UNSIGNED_INT, 0);
    }

    endRender();        
    }
}
