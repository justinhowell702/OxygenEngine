

package oxygenengine.graph;

import oxygenengine.graph.lighting.PointLight;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import static org.lwjgl.opengl.GL20.*;
import org.lwjgl.system.MemoryStack;
import oxygenengine.graph.lighting.DirectionalLight;
import oxygenengine.graph.lighting.SpotLight;

/**
 *
 * @author zeroxgen
 */
public class Shader {
    
    private final int m_progID;
    
    private int vertexShaderID;
    
    private int fragShaderID;
    
    private final Map<String, Integer> m_uniformMap;
    
    public Shader() throws Exception{
        m_progID = glCreateProgram();
        if(m_progID == 0){
            throw new Exception("Failed to create shader!");
        }
        m_uniformMap = new HashMap<>();
    }

    public void createUniform(String uniformName) throws Exception{
        int uniformLocation = glGetUniformLocation(m_progID, uniformName);
        if (uniformLocation < 0) {
            throw new Exception("Could not find uniform:" + uniformName);
        }
        m_uniformMap.put(uniformName, uniformLocation);
    }
    
    public void setUniform(String uniformName, Matrix4f value){
        try (MemoryStack stack = MemoryStack.stackPush()){
            FloatBuffer floatBuffer = stack.mallocFloat(16);
            value.get(floatBuffer);
            glUniformMatrix4fv(m_uniformMap.get(uniformName), false, floatBuffer);
        }
    }
    
    public void setUniform(String uniformName, int value){
        glUniform1i(m_uniformMap.get(uniformName), value);
    }
    
    public void setUniform(String uniformName, float value){
        glUniform1f(m_uniformMap.get(uniformName), value);
    }
    
    public void setUniform(String uniformName, Vector3f value){
        glUniform3f(m_uniformMap.get(uniformName), value.x, value.y, value.z);
    }
    
    public void setUniform(String uniformName, Vector4f value){
        glUniform4f(m_uniformMap.get(uniformName), value.x, value.y, value.z, value.w);
    }
    
    public void createVertexShader(String shaderCode) throws Exception{
        vertexShaderID = createShader(shaderCode, GL_VERTEX_SHADER);
    }
    
    public void createFragShader(String shaderCode) throws Exception{
        fragShaderID = createShader(shaderCode, GL_FRAGMENT_SHADER);
    }
    
    protected int createShader(String shaderCode, int shaderType) throws Exception{
        int shaderID = glCreateShader(shaderType);
        
        if(shaderID == 0){
            throw new Exception("Failed to create shader of type " + shaderType);
        }
        
        glShaderSource(shaderID, shaderCode);
        
        glCompileShader(shaderID);
        
        if(glGetShaderi(shaderID, GL_COMPILE_STATUS) == 0){
            throw new Exception("Failed to compile shader code for" + glGetShaderInfoLog(shaderID, 1024));
        }
        
        glAttachShader(m_progID, shaderID);
        
        return shaderID;
    }
    
    public void link() throws Exception{
        glLinkProgram(m_progID);
        
        if (glGetProgrami(m_progID, GL_LINK_STATUS) == 0){
            throw new Exception("Error linking shader code of " + glGetProgramInfoLog(m_progID, 1024));
        }
        
        if (vertexShaderID != 0){
            glDetachShader(m_progID, vertexShaderID);
        }
        
        if (fragShaderID != 0){
            glDetachShader(m_progID, fragShaderID);
        }
        
        glValidateProgram(m_progID);
        if(glGetProgrami(m_progID, GL_VALIDATE_STATUS) == 0){
            System.err.println("Warning validating Shader Code" + glGetProgramInfoLog(m_progID, 1024));
        }
    }
    
    public void bind(){
        glUseProgram(m_progID);
    }
    
    public void unbind(){
        glUseProgram(0);
    }
    
    public void cleanUp(){
        unbind();
        if(m_progID != 0){
            glDeleteProgram(m_progID);
        }
    }
    
    public void createPointLightListUniform(String uniformName, int size) throws Exception{
        for(int i = 0; i < size; i++){
            createPointLightUniform(uniformName + "[" + i + "]");
        }
    }
    
    public void createPointLightUniform(String uniformName) throws Exception{
        createUniform(uniformName + ".color");
        createUniform(uniformName + ".position");
        createUniform(uniformName + ".intensity");
        createUniform(uniformName + ".att.constant");
        createUniform(uniformName + ".att.linear");
        createUniform(uniformName + ".att.exponent");
    }
    
    public void createMaterialUniform(String uniformName) throws Exception{
    createUniform(uniformName + ".ambient");
    createUniform(uniformName + ".diffuse");
    createUniform(uniformName + ".specular");
    createUniform(uniformName + ".hasTexture");
    createUniform(uniformName + ".reflectance");        
    }
    
    public void setUniform(String uniformName, PointLight pLight){
        setUniform(uniformName + ".color", pLight.getColor());
        setUniform(uniformName + ".position", pLight.getPosition());
        setUniform(uniformName + ".intensity", pLight.getIntensity());
        PointLight.Attenuation att = pLight.getAttenuation();
        setUniform(uniformName + ".att.constant", att.getConstant());
        setUniform(uniformName + ".att.linear", att.getLinear());
        setUniform(uniformName + ".att.exponent", att.getExponent());
    }
    
    public void setUniform(String uniformName, Material material) {
        setUniform(uniformName + ".ambient", material.getAmbientColor());
        setUniform(uniformName + ".diffuse", material.getDiffuseColor());
        setUniform(uniformName + ".specular", material.getSpecularColor());
        setUniform(uniformName + ".hasTexture", material.isTextured() ? 1 : 0);
        setUniform(uniformName + ".reflectance", material.getReflectance());
    }
    
    public void createDirectionalLightUniform(String uniformName) throws Exception{
        createUniform(uniformName + ".color");
        createUniform(uniformName + ".direction");
        createUniform(uniformName + ".intensity");
    }
    
    public void createSpotLightListUniform(String uniformName, int size) throws Exception{
        for(int i = 0; i < size; i++){
            createSpotLightUniform(uniformName + "[" + i + "]");
        }
    }
    
    public void createSpotLightUniform(String uniformName) throws Exception{
        createPointLightUniform(uniformName + ".pl");
        createUniform(uniformName + ".conedir");
        createUniform(uniformName + ".cutoff");
    }
    
    public void setUniform(String uniformName, DirectionalLight dirLight){
        setUniform(uniformName + ".color", dirLight.getColor());
        setUniform(uniformName + ".direction", dirLight.getDirection());
        setUniform(uniformName + ".intensity", dirLight.getIntensity());
    }
    
    public void setUniform(String uniformName, SpotLight sLight){
        setUniform(uniformName + ".pl", sLight.getPointLight());
        setUniform(uniformName + ".conedir", sLight.getConeDirection());
        setUniform(uniformName + ".cutoff", sLight.getCutOff());
    }
    
    public void setUniform(String uniformName, SpotLight[] sLights){
        int numLights = sLights != null ? sLights.length : 0;
        for(int i = 0; i < numLights; i++){
            setUniform(uniformName, sLights[i], i);
        }
    }
    
    public void setUniform(String uniformName, SpotLight spotLight, int pos) {
        setUniform(uniformName + "[" + pos + "]", spotLight);
    }
    
    public void setUniform(String uniformName, PointLight[] pLights){
        int numLights = pLights != null ? pLights.length : 0;
        for(int i = 0; i < numLights ; i++){
            setUniform(uniformName, pLights[i], i);
        }
    }
    
    public void setUniform(String uniformName, PointLight pLight, int pos){
        setUniform(uniformName + "[" + pos + "]", pLight);
    }
    
}
