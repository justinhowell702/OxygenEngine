/*
 * 
 */
package oxygenengine.graph.lighting;

import org.joml.Vector3f;

/**
 *
 * @author zeroxgen
 */
public class SpotLight {
    
    private PointLight m_pLight;
    
    private Vector3f m_coneDirection;
    
    private float m_cutOff;
    
    public SpotLight(PointLight pLight, Vector3f coneDirection, float cutoff){
        m_pLight = pLight;
        m_coneDirection = coneDirection;
        m_cutOff = cutoff;
        
    }
    
    public SpotLight(SpotLight sLight){
        this(new PointLight(sLight.getPointLight()),
                new Vector3f(sLight.getConeDirection()),
                    0);
        
        setCutOff(sLight.getCutOff());
    }
    
    public PointLight getPointLight(){
        return m_pLight;
    }
    
    public void setPointLight(PointLight pLight){
        m_pLight = pLight;
    }
    
    public Vector3f getConeDirection(){
        return m_coneDirection;
    }
    
    public void setConeDirection(Vector3f coneDir){
        m_coneDirection = coneDir;
    }
    
    public float getCutOff(){
        return m_cutOff;
    }
    
    public void setCutOff(float cutOff){
        m_cutOff = cutOff;
    }
    
    public final void setCutOffAngle(float cutOffAngle){
        this.setCutOff((float)Math.cos(Math.toRadians(cutOffAngle)));
    }
}
