/*
 * 
 */
package oxygenengine.graph.lighting;

import org.joml.Vector3f;

/**
 *
 * @author zeroxgen
 */
public class PointLight {
    
    public static class Attenuation{
        private float constant;
        private float linear;
        private float exponent;
        
        public Attenuation(float constant, float linear, float exponent){
            this.constant = constant;
            this.linear = linear;
            this.exponent = exponent;
        }
        
        public float getConstant(){
            return constant;
        }
        public void setConstant(float constant){
            this.constant = constant;
        }
        public float getLinear(){
            return linear;
        }
        public void setLinear(float linear){
            this.linear = linear;
        }
        public float getExponent(){
            return exponent;
        }
        public void setExponent(){
            this.exponent = exponent;
        }
    }
    
    private Vector3f m_color;
    
    private Vector3f m_position;
    
    protected float m_intensity;
    
    private Attenuation m_atten;
    
    public PointLight(Vector3f color, Vector3f position, float intensity){
        m_atten = new Attenuation(1,0,0);
        m_color = color;
        m_intensity = intensity;
        m_position = position;
    }
    
    public PointLight(Vector3f color, Vector3f position, float intensity, Attenuation attenuation){
        this(color, position, intensity);
        m_atten = attenuation;
    }
    
    public PointLight(PointLight pLight){
        this(new Vector3f(pLight.getColor()), new Vector3f(pLight.getPosition()),
pLight.getIntensity(), pLight.getAttenuation());
    }
    
    public Vector3f getColor(){
        return m_color;
    }
    
    public void setColor(Vector3f color){
        m_color = color;
    }
    
    public Vector3f getPosition(){
        return m_position;
    }
    
    public void setPosition(Vector3f position){
        m_position = position;
    }
    
    public float getIntensity(){
        return m_intensity;
    }
    
    public void setIntensity(float intensity){
        m_intensity = intensity;
    }
    
    public Attenuation getAttenuation(){
        return m_atten;
    }
    
    public void setAttenuation(Attenuation attenuation){
        m_atten = attenuation;
    }
}
