/*
 * 
 */
package oxygenengine.graph.lighting;

import org.joml.Vector3f;

/**
 *
 * @author zeroxgen
 */
public class DirectionalLight {
    
    private Vector3f m_color;
    
    private Vector3f m_direction;
    
    private float m_intensity;
    
    public DirectionalLight(Vector3f color, Vector3f direction, float intensity){
        
        m_color = color;
        m_direction = direction;
        m_intensity = intensity;
        
    }
    
    public DirectionalLight(DirectionalLight light){
        this(new Vector3f(light.getColor()), new Vector3f(light.getDirection()), light.getIntensity());
    }
    
    public Vector3f getColor(){
        return m_color;
    }
    
    public void setColor(Vector3f color){
        m_color = color;
    }
    
    public Vector3f getDirection(){
        return m_direction;
    }
    
    public void setDirection(Vector3f direction){
        m_direction = direction;
    }
    
    public float getIntensity(){
        return m_intensity;
    }
    
    public void setIntensity(float intensity){
        m_intensity = intensity;
    }
}
