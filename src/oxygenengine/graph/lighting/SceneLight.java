/*
 * 
 */
package oxygenengine.graph.lighting;

import org.joml.Vector3f;

/**
 *
 * @author zeroxgen
 */
public class SceneLight {
    
    private Vector3f m_aLight;
    
    private PointLight[] m_pLightList;
    
    private SpotLight[] m_sLightList;
    
    private DirectionalLight m_dLight;
    
    public Vector3f getAmbientLight(){
        return m_aLight;
    }
    
    public void setAmbientLight(Vector3f aLight){
        m_aLight = aLight;
    }
    
    public PointLight[] getPointLightList(){
        return m_pLightList;
    }
    
    public void setPointLightList(PointLight[] pLights){
        m_pLightList = pLights;
    }
    
    public SpotLight[] getSpotLightList(){
        return m_sLightList;
    }
    
    public void setSpotLightList(SpotLight[] sLights){
        m_sLightList = sLights;
    }
    
    public DirectionalLight getDirectionalLight(){
        return m_dLight; 
    }
    
    public void setDirectionalLight(DirectionalLight dLight){
        m_dLight = dLight;
    }
}
