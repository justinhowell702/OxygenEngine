
package oxygenengine;

import org.lwjgl.glfw.*;

import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import java.nio.*;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;



/**
 *
 * @author zeroxgen
 */
public class Window {
    
    //window handle
    private long m_windowHandle;
    private WindowSettings m_wSettings;

    
    public void init(WindowSettings wSettings){
        GLFWErrorCallback.createPrint(System.err).set();        
        if( !glfwInit() )
            throw new IllegalStateException("Unable to intialize GLFW.");
        
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        
        m_wSettings = wSettings;
        
        m_windowHandle = glfwCreateWindow(wSettings.m_width , wSettings.m_height, wSettings.m_title, NULL, NULL);
		if ( m_windowHandle == NULL )
			throw new RuntimeException("Failed to create the GLFW window");
        
        glfwSetKeyCallback(m_windowHandle, (window, key, scancode, action, mods) -> {
        if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(m_windowHandle, true);
        
        });
        
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1);
            IntBuffer pHeight = stack.mallocInt(1);
            
            glfwGetWindowSize(m_windowHandle, pWidth, pHeight);
            
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
            glfwSetWindowPos(
		m_windowHandle,
			(vidmode.width() - pWidth.get(0)) / 2,
			(vidmode.height() - pHeight.get(0)) / 2
	);
        }
        
        glfwMakeContextCurrent(m_windowHandle);
        
        glfwSwapInterval(1);
        
        glfwShowWindow(m_windowHandle);
        
        GL.createCapabilities();
        
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  
        
        //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    }
       
    
    public boolean checkClose(){
        return glfwWindowShouldClose(m_windowHandle);
    }
    
    public void render(){
          //  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                    
            glfwSwapBuffers(m_windowHandle);    
            
//            glfwPollEvents();
    }
    
    public void update(){
            
            glfwPollEvents();
        
    }
    
    public boolean isKeyPressed(int keyCode) {
        return glfwGetKey(m_windowHandle, keyCode) == GLFW_PRESS;
    }
    
    public void cleanUp(){
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }
    
    public int getWindowWidth(){
        return m_wSettings.m_width;
    }
    
    public int getWindowHeight(){
        return m_wSettings.m_height;
    }
    
    public long getWindowHandle(){
        return m_windowHandle;
    }
    
}
