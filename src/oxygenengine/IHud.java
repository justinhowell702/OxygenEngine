/*
 * 
 */
package oxygenengine;

import oxygenengine.items.GameItem;
import java.util.List;
import oxygenengine.hud.HudItem;
import oxygenengine.hud.TextItem;

/**
 *
 * @author zeroxgen
 */
public interface IHud {
    
    GameItem[] getGameItems();
    
    TextItem[] getTextItems();
    
    List<HudItem> getRenderStack();
    
    default void cleanup() {
        GameItem[] gItems = getGameItems();
        for(GameItem gItem : gItems){
            gItem.getMesh().cleanUp();
        }
    }
    
}
