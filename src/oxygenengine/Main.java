/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxygenengine;

import dummyGame.dummyGame;

/**
 *
 * @author zeroxgen
 */
public class Main {
    
    
    public static void main(String[] args) {
        WindowSettings wSettings = new WindowSettings();
        IGameLogic gLogic = new dummyGame();
        OxygenEngine eng = new OxygenEngine(wSettings ,gLogic);
        eng.run();
        
    }
}
