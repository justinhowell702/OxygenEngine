
package oxygenengine;

import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * @author zeroxgen
 */

public class OxygenEngine {

    public static final double FRAME_CAP = 5000.0;
    Window mWindow = new Window();
    Time time = new Time();
    private final IGameLogic gLogic;
    private final MouseInput mInput;
    WindowSettings wSettings = null;

    public OxygenEngine(WindowSettings bootSettings,IGameLogic gLogic){
        this.gLogic = gLogic;
        wSettings = bootSettings;
        mInput = new MouseInput();
    }
    
    public void run(){
        mWindow.init(wSettings);
        mInput.init(mWindow);
        try{
        gLogic.init(mWindow);
        } catch (Exception ex) {
            Logger.getLogger(OxygenEngine.class.getName()).log(Level.SEVERE, null, ex);
        }
        gameLoop();
    }
    
    public void gameLoop(){
        boolean runEng = true;
        
        int frames = 0;
        long frameCounter = 0;
        
        final double frameTime = 1.0 / FRAME_CAP;
        
        long lastTime = Time.getTime();
        double unprocTime = 0;
        

        
        while (runEng){
            long startTime = Time.getTime();
            long passedTime = startTime - lastTime;
            lastTime = startTime;
            
            boolean renderFrame = false;

            unprocTime += passedTime / (double) Time.SECOND;
            frameCounter += passedTime;

            input();
            
            while(unprocTime > frameTime){
                renderFrame = true;
                
                unprocTime -= frameTime;
                
                if(mWindow.checkClose()){
                    runEng = false;
                }
                if(frameCounter >= Time.SECOND){
                    System.out.println(frames);
                    frames = 0;
                    frameCounter = 0;
                }
            }            
            
            if(renderFrame){
                frames++;
                update();
                
                render();
            }else{
                try{
                    Thread.sleep(1);
                }catch (InterruptedException ie){
                    ie.printStackTrace();
                }
            }
            
            if(!((frames % 5) > 0)){
                frame5update();
            }
            
        }
        
        mWindow.cleanUp();

    }
    
    
    private void input(){
        mInput.input(mWindow);
        gLogic.input(mWindow, mInput);
    }
    
    private void update(){
        gLogic.update(mInput);
        mWindow.update();
    }
    
    private void frame5update(){
        gLogic.frame5update();
    }
    
    private void render(){
        gLogic.render(mWindow);
        mWindow.render();
        //mWindow.update();
    }
    
    protected void cleanUp(){
        gLogic.cleanUp();
    }
    
}
